const http = require('http');
const httpStatusCodes = require('http').STATUS_CODES;
const { v4: uuidv4 } = require('uuid');

try {

    let httpServer = http.createServer((req, res) => {

        if (req.method === 'GET') {
            if (req.url == "/html") {
                res.writeHead(200)
                res.end(
                    `<!DOCTYPE html>
                    <html>
                    <head>
                    </head>
                    <body>
                        <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
                        <p> - Martin Fowler</p>
                    
                    </body>
                    </html>`
                );
            }

            else if (req.url == "/json") {
                res.writeHead(200,{"Content-type" : "json"})
                res.end(
                    `
                    {
                        "slideshow": {
                          "author": "Yours Truly",
                          "date": "date of publication",
                          "slides": [
                            {
                              "title": "Wake up to WonderWidgets!",
                              "type": "all"
                            },
                            {
                              "items": [
                                "Why <em>WonderWidgets</em> are great",
                                "Who <em>buys</em> WonderWidgets"
                              ],
                              "title": "Overview",
                              "type": "all"
                            }
                          ],
                          "title": "Sample Slide Show"
                        }
                      }
                    `
                );
            }
            else if (req.url == "/uuid") {
                res.writeHead(200)

                let uuidVal = uuidv4();
                let uuidObj = { uuid: uuidVal };
                res.end(
                    JSON.stringify(uuidObj)
                );
            }
            if (req.url.includes("/status")) {
                let url = req.url;
                let splittedUrl = req.url.split("/status/");
                let code = splittedUrl[1]
                if (!httpStatusCodes[code]) {
                    res.writeHead("404");
                    res.end("Invalid Status Code !")
                }
                else {
                    if(Number(code) >= 200){
                        res.writeHead(code);
                    }
                    res.end(httpStatusCodes[code]);
                }
            }
            else if (req.url.includes("/delay")) {
                let url = req.url;
                let splittedUrl = req.url.split("/delay/");
                let delayTime = Number(splittedUrl[1]);

                if (delayTime || delayTime == 0) {
                    setTimeout(() => {
                        res.end(httpStatusCodes[200]);
                    }, delayTime);
                }
                else {
                    res.end("Pass valid delay time !")
                }
            }
            else
            {
                res.end("Error 404 : NOT FOUND")
            }
        }
    })

    httpServer.listen(8080);


} catch (error) {
    console.log("Something Went Wrong !");
}